(function ($) {

/**
 * The Theta Drupal Behavior to apply AJAX browsing on top of links.
 */
Drupal.behaviors.theta = {
  attach: function (context, settings) {
    $('body').once('hurl', function() {
      $.hurl({monitor:false});
    }).bind('hurl', function(event) {
      var link = $("body").data("hurl").link;

      // Make the AJAX request to load the new content.
      $.ajax({
        dataType: 'html',
        url: Drupal.settings.basePath + (link.go || ''),
        success: function(data) {
          // Replace each region with the new content.
          $.each(Drupal.settings.theta, function(index, region) {
            var replacement = $('#' + region, data);
            $('#' + region).replaceWith(replacement);
          });

          // @TODO: Load any new JavaScript/CSS?
          // @TODO: What if the region sizes change with Delta?

          // Attach all the behaviors for any new content.
          Drupal.attachBehaviors(document, Drupal.settings);
        },
        cache: false,
        type: "GET"
      });
      
    });

    // Take a look at each region provided by Omega.
    $.each(settings.theta || {}, function(index, region) {
      // Process all links in the regions.
      $('#' + region + ' a').once('theta').click(function(event) {
        // Retrieve the URL for the link.
        var href = $(this).attr('href');
        // Only act on links that have a href.
        if (href !== false && href.indexOf(settings.basePath) >= 0) {
          // Retrieve the true URL and update the URL hash.
          href = href.replace(settings.basePath, '');
          $.hurl("update", {"go": href});

          return false;
        }
      });
    });

    // @TODO: If there is already a hash bang URL, load up that content.
    // @TODO: Hashchange event for back/forward.
  }
};

 

})(jQuery);
